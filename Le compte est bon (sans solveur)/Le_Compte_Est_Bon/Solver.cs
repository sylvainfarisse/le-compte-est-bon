﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Le_Compte_Est_Bon
{
    class Solver
    {
        private string display;
        private int result;

        public Solver()
        {

            this.Start();
        }

        public void Start()
        {
            Console.WriteLine(" Le compte est bon :");
            Console.WriteLine(" Pour Jouer (chercher le calcul) ==> 1");
            Console.WriteLine(" Solveur ==>  2");

            switch (Console.ReadLine())
            {
                case "1":
                    Init();
                    break;

                case "2":
                    Solv();
                    break;

                default:
                    Console.WriteLine(" Veullez taper un chiffre "); ;
                    Console.ReadLine();
                    break;
            }
        }


        private void Init()
        {
            Console.WriteLine(" Les valeurs sont :");
            Console.ReadLine();

            Random aleatoire = new Random();
            int[] values = new int[6];

            for (int k = 0; k < 6; k++)
            {
                values.SetValue(aleatoire.Next(1, 50), k);
                Console.WriteLine(values[k]);
            }

            this.display = values[0].ToString();
            this.result = values[0];
            for (int k = 1; k < 6; k++)
            {
                this.Operation(values[k]);
            }



            Console.WriteLine(" Le résultat attendu est : ");
            Console.WriteLine(result);
            Console.ReadLine();
            Console.WriteLine(" L'une des solution possible est :");
            Console.WriteLine(this.display + " = " + this.result);
            Console.ReadLine();
        }

        private void Operation(int value)
        {
            Random aleatoire = new Random();
            int Operator = aleatoire.Next(1, 4);

            switch (Operator)
            {
                case 1:
                    //calcul
                    this.result += value;
                    this.display += " + " + value;
                    break;
                case 2:
                    //calcul
                    this.result -= value;
                    this.display += " - " + value;
                    break;
                case 3:
                    //calcul
                    this.result /= value;
                    this.display += " / " + value;
                    break;
                case 4:
                    //calcul
                    this.result *= value;
                    this.display += " * " + value;
                    break;
            }
        }

        private void Solv()
        {

        }
    }
}
