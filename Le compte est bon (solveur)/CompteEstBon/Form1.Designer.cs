﻿namespace CompteEstBon
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PanelPlaquettes = new System.Windows.Forms.FlowLayoutPanel();
            this.btnPlus = new System.Windows.Forms.Button();
            this.lblPlaquettes = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtChiffre = new System.Windows.Forms.TextBox();
            this.txtResultat = new System.Windows.Forms.TextBox();
            this.btnCalcul = new System.Windows.Forms.Button();
            this.btnAuRevoir = new System.Windows.Forms.Button();
            this.lblResInt = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNbCombi = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblNbEntreesDic = new System.Windows.Forms.Label();
            this.ChkDic = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblSecondes = new System.Windows.Forms.Label();
            this.btnMoins = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.tipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.lbli = new System.Windows.Forms.Label();
            this.lblj = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PanelPlaquettes
            // 
            this.PanelPlaquettes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelPlaquettes.Location = new System.Drawing.Point(83, 58);
            this.PanelPlaquettes.Name = "PanelPlaquettes";
            this.PanelPlaquettes.Size = new System.Drawing.Size(443, 59);
            this.PanelPlaquettes.TabIndex = 0;
            // 
            // btnPlus
            // 
            this.btnPlus.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnPlus.Location = new System.Drawing.Point(15, 45);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(20, 23);
            this.btnPlus.TabIndex = 34;
            this.btnPlus.TabStop = false;
            this.btnPlus.Text = "ñ";
            this.btnPlus.UseVisualStyleBackColor = true;
            this.btnPlus.Click += new System.EventHandler(this.btnPlus_Click);
            // 
            // lblPlaquettes
            // 
            this.lblPlaquettes.AutoSize = true;
            this.lblPlaquettes.Location = new System.Drawing.Point(41, 63);
            this.lblPlaquettes.Name = "lblPlaquettes";
            this.lblPlaquettes.Size = new System.Drawing.Size(13, 13);
            this.lblPlaquettes.TabIndex = 3;
            this.lblPlaquettes.Text = "6";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Chiffre :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Resultat :";
            // 
            // txtChiffre
            // 
            this.txtChiffre.Location = new System.Drawing.Point(83, 133);
            this.txtChiffre.Name = "txtChiffre";
            this.txtChiffre.Size = new System.Drawing.Size(100, 20);
            this.txtChiffre.TabIndex = 11;
            // 
            // txtResultat
            // 
            this.txtResultat.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultat.Location = new System.Drawing.Point(83, 215);
            this.txtResultat.MaxLength = 300000;
            this.txtResultat.Multiline = true;
            this.txtResultat.Name = "txtResultat";
            this.txtResultat.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResultat.Size = new System.Drawing.Size(374, 215);
            this.txtResultat.TabIndex = 7;
            this.txtResultat.TabStop = false;
            // 
            // btnCalcul
            // 
            this.btnCalcul.Location = new System.Drawing.Point(382, 449);
            this.btnCalcul.Name = "btnCalcul";
            this.btnCalcul.Size = new System.Drawing.Size(75, 23);
            this.btnCalcul.TabIndex = 12;
            this.btnCalcul.Text = "Calcul";
            this.btnCalcul.UseVisualStyleBackColor = true;
            this.btnCalcul.Click += new System.EventHandler(this.btnCalcul_Click);
            // 
            // btnAuRevoir
            // 
            this.btnAuRevoir.Location = new System.Drawing.Point(83, 449);
            this.btnAuRevoir.Name = "btnAuRevoir";
            this.btnAuRevoir.Size = new System.Drawing.Size(75, 23);
            this.btnAuRevoir.TabIndex = 9;
            this.btnAuRevoir.TabStop = false;
            this.btnAuRevoir.Text = "Au revoir";
            this.btnAuRevoir.UseVisualStyleBackColor = true;
            this.btnAuRevoir.Click += new System.EventHandler(this.btnAuRevoir_Click);
            // 
            // lblResInt
            // 
            this.lblResInt.AutoSize = true;
            this.lblResInt.Location = new System.Drawing.Point(308, 136);
            this.lblResInt.MinimumSize = new System.Drawing.Size(10, 10);
            this.lblResInt.Name = "lblResInt";
            this.lblResInt.Size = new System.Drawing.Size(16, 13);
            this.lblResInt.TabIndex = 35;
            this.lblResInt.Text = " 0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(220, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "Valeur proche :";
            this.tipInfo.SetToolTip(this.label1, "Solution la plus proche.");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(332, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "NbCalculs :";
            this.tipInfo.SetToolTip(this.label4, "Nombre total de calcul.\r\nNota : ce nombre diminue si on active la table de Hashag" +
                    "e.");
            // 
            // lblNbCombi
            // 
            this.lblNbCombi.AutoSize = true;
            this.lblNbCombi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbCombi.Location = new System.Drawing.Point(418, 165);
            this.lblNbCombi.MinimumSize = new System.Drawing.Size(100, 0);
            this.lblNbCombi.Name = "lblNbCombi";
            this.lblNbCombi.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblNbCombi.Size = new System.Drawing.Size(100, 13);
            this.lblNbCombi.TabIndex = 40;
            this.lblNbCombi.Text = "0";
            this.lblNbCombi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblNbCombi.Click += new System.EventHandler(this.lblNbCombi_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(332, 183);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 39;
            this.label7.Text = "NbEntréesTab :";
            this.tipInfo.SetToolTip(this.label7, "Affiche le nombre d\'entrées dans la table de Hashage.\r\n");
            // 
            // lblNbEntreesDic
            // 
            this.lblNbEntreesDic.AutoSize = true;
            this.lblNbEntreesDic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbEntreesDic.Location = new System.Drawing.Point(418, 183);
            this.lblNbEntreesDic.MinimumSize = new System.Drawing.Size(100, 0);
            this.lblNbEntreesDic.Name = "lblNbEntreesDic";
            this.lblNbEntreesDic.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblNbEntreesDic.Size = new System.Drawing.Size(100, 13);
            this.lblNbEntreesDic.TabIndex = 40;
            this.lblNbEntreesDic.Text = "0";
            this.lblNbEntreesDic.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ChkDic
            // 
            this.ChkDic.AutoSize = true;
            this.ChkDic.Location = new System.Drawing.Point(394, 16);
            this.ChkDic.Name = "ChkDic";
            this.ChkDic.Size = new System.Drawing.Size(120, 17);
            this.ChkDic.TabIndex = 42;
            this.ChkDic.Text = "Table Hash Activée";
            this.tipInfo.SetToolTip(this.ChkDic, "Table qui enregistre les données déjà calculées. \r\nAméliore les performances. \r\nE" +
                    "vite les solutions redondantes");
            this.ChkDic.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(179, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Temps Ecoulé :";
            // 
            // lblSecondes
            // 
            this.lblSecondes.AutoSize = true;
            this.lblSecondes.Location = new System.Drawing.Point(259, 17);
            this.lblSecondes.Name = "lblSecondes";
            this.lblSecondes.Size = new System.Drawing.Size(49, 13);
            this.lblSecondes.TabIndex = 44;
            this.lblSecondes.Text = "00:00:00";
            // 
            // btnMoins
            // 
            this.btnMoins.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnMoins.Location = new System.Drawing.Point(15, 74);
            this.btnMoins.Name = "btnMoins";
            this.btnMoins.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnMoins.Size = new System.Drawing.Size(20, 23);
            this.btnMoins.TabIndex = 33;
            this.btnMoins.TabStop = false;
            this.btnMoins.Text = "ò";
            this.btnMoins.UseVisualStyleBackColor = true;
            this.btnMoins.Click += new System.EventHandler(this.btnMoins_Click);
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(83, 12);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 45;
            this.btnStop.Text = "STOP !";
            this.tipInfo.SetToolTip(this.btnStop, "Stoppe le calcul en cours");
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // tipInfo
            // 
            this.tipInfo.IsBalloon = true;
            this.tipInfo.ShowAlways = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(418, 135);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 14);
            this.label5.TabIndex = 41;
            this.label5.Text = "i:";
            this.tipInfo.SetToolTip(this.label5, "Position du 1er élement i par rapport au 2eme élement j.\r\nCalcul de pl[i] (+-x:)p" +
                    "l[j]\r\nNota : montre que le i et j du premier niveau\r\n");
            // 
            // lbli
            // 
            this.lbli.AutoSize = true;
            this.lbli.Location = new System.Drawing.Point(435, 136);
            this.lbli.Name = "lbli";
            this.lbli.Size = new System.Drawing.Size(13, 13);
            this.lbli.TabIndex = 37;
            this.lbli.Text = "0";
            // 
            // lblj
            // 
            this.lblj.AutoSize = true;
            this.lblj.Location = new System.Drawing.Point(472, 136);
            this.lblj.Name = "lblj";
            this.lblj.Size = new System.Drawing.Size(13, 13);
            this.lblj.TabIndex = 38;
            this.lblj.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(454, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 14);
            this.label6.TabIndex = 41;
            this.label6.Text = "j:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(546, 496);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.lblSecondes);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ChkDic);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblNbEntreesDic);
            this.Controls.Add(this.lblNbCombi);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblj);
            this.Controls.Add(this.lbli);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblResInt);
            this.Controls.Add(this.btnAuRevoir);
            this.Controls.Add(this.btnCalcul);
            this.Controls.Add(this.txtResultat);
            this.Controls.Add(this.txtChiffre);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblPlaquettes);
            this.Controls.Add(this.btnPlus);
            this.Controls.Add(this.btnMoins);
            this.Controls.Add(this.PanelPlaquettes);
            this.Name = "Form1";
            this.Text = "Compte est Bon";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel PanelPlaquettes;
         private System.Windows.Forms.Button btnPlus;
        private System.Windows.Forms.Label lblPlaquettes;
        private System.Windows.Forms.Button btnMoins;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtChiffre;
        public System.Windows.Forms.TextBox txtResultat;
        private System.Windows.Forms.Button btnCalcul;
        private System.Windows.Forms.Button btnAuRevoir;
        public System.Windows.Forms.Label lblResInt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lblNbCombi;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label lblNbEntreesDic;
        private System.Windows.Forms.CheckBox ChkDic;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label lblSecondes;
        private System.Windows.Forms.ToolTip tipInfo;
        public System.Windows.Forms.Label lbli;
        public System.Windows.Forms.Label lblj;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        protected internal System.Windows.Forms.Button btnStop;

    }
}

